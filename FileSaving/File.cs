﻿using System;
using System.Text;

namespace FileSaving
{
    public class File
    {
        private readonly StringBuilder content;

        public string Content => content.ToString();

        public File(string initialContent = "")
        {
            this.content = new StringBuilder(initialContent);
        }

        public void Append(string appendix)
        {
            content.Append(appendix);
        }

        public void Clear()
        {
            content.Clear();
        }

        public void Insert(int position, string value)
        {
            content.Insert(position, value);
        }

        public void Remove(int position, int howMany)
        {
            content.Remove(position, howMany);
        }

        public void Replace(string oldValue, string newValue)
        {
            content.Replace(oldValue, newValue);
        }
    }
}
