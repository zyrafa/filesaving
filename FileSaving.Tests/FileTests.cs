﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shouldly;

namespace FileSaving.Tests
{
    public class FileTests
    {
        [TestClass]
        public class Append
        {
            [TestMethod]
            public void Appends_given_string_to_the_end_of_file()
            {
                AssertStringProperlyAppended("", "ghi");
                AssertStringProperlyAppended("abcdef", "");
                AssertStringProperlyAppended("abcdef", "ghi");
            }

            private void AssertStringProperlyAppended(string initialValue, string addition)
            {
                var file = new File(initialValue);

                file.Append(addition);

                file.Content.ShouldBe(initialValue + addition);
            }
        }

        [TestClass]
        public class Clear
        {
            [TestMethod]
            public void Clears_file_content()
            {
                AssertContentProperlyCleared("");
                AssertContentProperlyCleared("abcdef");
            }

            private void AssertContentProperlyCleared(string initialValue)
            {
                var file = new File(initialValue);

                file.Clear();

                file.Content.ShouldBe(string.Empty);
            }
        }

        [TestClass]
        public class Insert
        {
            [TestMethod]
            public void Puts_given_value_into_given_position()
            {
                AssertContentProperlyInserted("", "abc", 0, "abc");
                AssertContentProperlyInserted("defghi", "abc", 0, "abcdefghi");
                AssertContentProperlyInserted("abcghi", "def", 3, "abcdefghi");
            }

            private void AssertContentProperlyInserted(string initialValue, string valueToInsert, int position, string expected)
            {
                var file = new File(initialValue);

                file.Insert(position, valueToInsert);

                file.Content.ShouldBe(expected);
            }
        }
        
        [TestClass]
        public class Remove
        {
            [TestMethod]
            public void Removes_given_number_of_chars_starting_on_given_position()
            {
                AssertProperlyRemoves("abcdef", 3, 3, "abc");
                AssertProperlyRemoves("abcdef", 0, 3, "def");
            }

            private void AssertProperlyRemoves(string initialValue, int position, int howMany, string expected)
            {
                var file = new File(initialValue);

                file.Remove(position, howMany);

                file.Content.ShouldBe(expected);
            }
        }

        [TestClass]
        public class Replace
        {
            [TestMethod]
            public void Replace_replaces_given_string_in_file_content()
            {
                AssertProperlyReplaces("", "abc", "def", "");
                AssertProperlyReplaces("abcdef", "ghi", "jkl", "abcdef");
                AssertProperlyReplaces("abcghi", "ghi", "def", "abcdef");
            }

            private void AssertProperlyReplaces(string initialValue, string oldValue, string newValue, string expected)
            {
                var file = new File(initialValue);

                file.Replace(oldValue, newValue);

                file.Content.ShouldBe(expected);
            }
        }
    }
}
